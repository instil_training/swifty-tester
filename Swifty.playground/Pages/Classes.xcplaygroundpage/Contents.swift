/*: defining classes; initialising instances; structs; enums
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Classes
 */

//: ## Defining classes

//: ## Extending existing classes

//: ## Initialisers

//: ## Methods

//: ## Properties

//: ## Structs

//: ## Enums

//: [Previous](@previous) - [Next](@next)
