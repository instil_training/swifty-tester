/*:
 ![Instil Logo](instil-logo-header.png)
 ___
 # The Swift Language
 
 ## Table of Contents

 * [Variables](Variables)
 * [Optionals](Optionals)
 * [Functions](Functions)
 * [Classes](Classes)
 * [Protocols and Extensions](Protocols)
 * [Closures](Closures)
 * [Error handling](Errors)
 */
