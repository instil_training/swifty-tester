/*: lambdas; defining blocks; passing blocks as arguments;
 ![Instil Logo](instil-logo-header.png)
 ### The Swift Language
 ___
 [Table of Contents](TOC)
 # Protocols and extensions
 */

//: ## Defining and implementing protocols

//: ## Adding functionality to existing classes

//: [Previous](@previous) - [Next](@next)
